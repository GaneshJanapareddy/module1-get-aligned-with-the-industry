# Product study-1

### Product name
Vision AI

### Product link
https://cloud.google.com/vision/

### Product Short description
This tool, part of the Google Cloud Platform, enables developers to integrate image recognition and object detection capabilities using simple API (Application Programming Interface) calls. Google is leading the way in computer vision and you can be sure that'll benefit from the most powerful vision machine learning models using this tool. It can extract useful insights and text out of the examined images as well as integrate the functions of their reverse image search engine making it one of the most versatile and flexible tools.

### Product is combination of features

- Object detection
- Person Detection
- ID Recognition


### Product is provided by which company?
Google


# Product study-2

### Product name
Amazon Rekognition

### Product link
https://aws.amazon.com/rekognition/?blog-cards.sort-by=item.additionalFields.createdDate&blog-cards.sort-order=desc

### Product Short description
Amazon Rekognition makes it easy to add image and video analysis to your applications using proven, highly scalable, deep learning technology that requires no machine learning expertise to use. With Amazon Rekognition, you can identify objects, people, text, scenes, and activities in images and videos, as well as detect any inappropriate content. Amazon Rekognition also provides highly accurate facial analysis and facial search capabilities that you can use to detect, analyze, and compare faces for a wide variety of user verification, people counting, and public safety use cases.

### Product is combination of features

- Object detection
- Person Detection


### Product is provided by which company?
Amazon